// HomeWork_19_5

// �� ���� ��������� ���������� 

#include <iostream>
using namespace std;
class Animal // base class
{
public:    
    virtual void Voice()
    {
        cout << "Animal" << endl;
    }
}; 
class Dog : public Animal
{
public:    
    void Voice() override
    {
        cout << "Woof!" << endl;
    }
};
class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow!" << endl;
    }
};
class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "Muuu" << endl;
    }
};
class Horse : public Animal
{
public:
    void Voice() override
    {
        cout << "Igogo!" << endl;
    }
};
class Ram : public Animal
{
public:
    void Voice() override
    {
        cout << "Beee" << endl;
    }
};
class Goat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meeee" << endl;
    }
};

int main()
{
    Animal* class1 = new Dog;
    Animal* class2 = new Cat;
    Animal* class3 = new Cow;
    Animal* class4 = new Horse;
    Animal* class5 = new Ram;
    Animal* class6 = new Goat;
    Animal* array[6] = { class1, class2, class3, class4, class5, class6 }; 
    for (int i = 0; i < 6; i++) 
    {
        array[i]->Voice();
    }
    return 0;
}

