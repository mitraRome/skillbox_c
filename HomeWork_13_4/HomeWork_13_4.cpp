#include <iostream>
#include "SquareSum.h"
#include <iomanip>

int main()
{
    int a = 10;
    int b = 2;
    int c = SquareSum(a, b);

    double s = 5e-10;

    std::cout << "Squared sum of " << a << " and " << b << " = " << c << std::endl;

    std::cout << std::fixed << std::setprecision(10) << s;

    return 0;
}
